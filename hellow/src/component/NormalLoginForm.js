import React from 'react'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import TextArea from './container/TextAria'


class NormalLoginForm extends React.Component {
  
  
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="نام و نام خانوادگی"
            />,
          )}
        </Form.Item>
        <Form.Item>
          
         <TextArea rows={5} 
         placeholder=" آدرس خود را  وارد کنید"
         autosize={8000}
         /> 
         
        </Form.Item>


        <Form.Item>
          {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })
          (<Checkbox>Remember me</Checkbox>)}
          <br/>
          <a className="login-form-forgot" href="">
            Forgot password
          </a>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
          Or <a href="">register now!</a>
        </Form.Item>
      </Form>
    );
  }
}


export default Form.create()(NormalLoginForm);









import React from 'react';
import './App.css';
import CostumLayout from './component/container/Layout'
import ArticleList from './component/ArticleList'
function App() {
  return (
    <div className="App">
      <CostumLayout >
      
      <ArticleList />

      </CostumLayout>
          </div>
  );
}

export default App;
